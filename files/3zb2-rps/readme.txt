This is not a 100% faithful port of Rock Paper Scissor (RPS) v1.3.2 to 3rd-Zigock Bot II for Quake 2.
The following RPS features are not implemented (and probably never will):

	1. Standard Logging
	2. GibStats Logging

Major changes in this port:
	- Teamskins:
		You can now use CTF team skins by setting the 'teamskins' cvar.
		With this setting on Quad Damage and Invulnerability players become more visible.